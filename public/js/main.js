$(document).ready(function() {

    let _burgerIcon = document.getElementById('burger-icon');
    let _floatingNav = document.getElementById('floating-nav');
    let _closingCross = document.getElementById('closing-cross');

    _burgerIcon.addEventListener('click', function() {
        _burgerIcon.classList.add('hidden');
        _floatingNav.classList.remove('hidden');
    });
    _closingCross.addEventListener('click', function() {
        _floatingNav.classList.add('hidden');
        _burgerIcon.classList.remove('hidden');
    });

    // De base, on cache l'élément
    $("#to-top").hide();

    // Fonction sur le scroll
    $(function () {
        $(window).scroll(function () {

            //On inverse : on commence par si on est a moins de 400
            if ($(this).scrollTop() < 150) {
                $('#to-top').fadeOut(500); // on cache ( i.e on vient de remonter trop haut)
            } else if ($(this).scrollTop() < 650) // on est a moins de 650 ( mais plus de 400)
            {
                $('#to-top').fadeIn(500); //on montre #to-top
            }

        });
    });
});