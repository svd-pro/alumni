<?php
namespace App\Controller;

use App\Service\Avatar\SvgAvatarFactory;
use App\Service\Helpers\FileSystemHelper;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AvatarController extends AbstractController
{

    private $svgAvatarFactory;
    private $fileSystemHelper;
    private $uploadDir;

    public function __construct($uploadDir,
                                SvgAvatarFactory $svgAvatarFactory,
                                FileSystemHelper $fileSystemHelper)
    {
        $this->uploadDir = $uploadDir;
        $this->svgAvatarFactory = $svgAvatarFactory;
        $this->fileSystemHelper = $fileSystemHelper;
    }

    /**
     * @Route("/avatar", name="avatar.get")
     */
    public function getAvatar()
    {
        $svg = $this->svgAvatarFactory->getAvatar(2, 5);
        $filePath = $this->uploadDir . '/' . SvgAvatarFactory::AVATAR_DIR;

        $this->fileSystemHelper->setHalfRandomFileName('avatar', 'svg');
        $this->fileSystemHelper->setFilePath($filePath); // 'uploads/avatars'
        $this->fileSystemHelper->saveFile($svg);

        $filename = $this->fileSystemHelper->getName();
//        return new Response($svg);
        return $this->render('avatar.html.twig', ['filename' => $filename]);
    }

}