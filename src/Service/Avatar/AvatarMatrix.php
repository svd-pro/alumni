<?php
namespace App\Service\Avatar;

class AvatarMatrix {

    const DEFAULT_SIZE = 9;
    const DEFAULT_COLORS = ['black', 'white'];

    private $matrix;
    private $size;
    private $colors;
    
    public function __construct($size = self::DEFAULT_SIZE, $colors = self::DEFAULT_COLORS) {
        $this->matrix = [];
        $this->size = $size;
        $this->colors = $colors;
        $this->build();
    }
    
    // BUILD
    public function build() {
        for ($i = 0; $i < $this->size; $i++) {
            $this->matrix[$i] = [];
            for ($i2 = 0; $i2 < $this->size / 2; $i2++) {
                $color = $this->colors[rand(0, (count($this->colors) - 1))];
                $this->matrix[$i][$i2] = $color;
                $this->matrix[$i][($this->size - $i2 - 1)] = $color;
            }
        }
    }

    // SETTERS
    public function setSize($size) {
        $this->size = $size;
    }

    public function setColors($colors) {
        $this->colors = $colors;
    }

    // GETTERS
    public function getMatrix() {
        return $this->matrix;
    }

    public function getSize() {
        return $this->size;
    }

}