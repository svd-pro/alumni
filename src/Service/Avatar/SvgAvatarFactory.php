<?php
namespace App\Service\Avatar;

use App\Service\Tools\ColorsProvider;

class SvgAvatarFactory {

    const AVATAR_DIR = 'avatars';
    private $template;

    public function __construct($template)
    {
        $this->template = $template;
    }

    public function getAvatar(int $nbOfColors, int $size)
    {
        $setOfColors = ColorsProvider::getRandomColors($nbOfColors);
        $avatar = new AvatarMatrix($size, $setOfColors);
        $svgAvatarManager = new SvgAvatarManager($this->template);
        $svgAvatarManager->setAvatar($avatar);
        $svgAvatarManager->writeSvg();

        return $svgAvatarManager->drawSvg();
    }

}