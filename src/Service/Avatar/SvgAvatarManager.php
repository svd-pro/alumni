<?php
namespace App\Service\Avatar;

class SvgAvatarManager {

    const DEFAULT_ORIGIN = ['x' => 0, 'y' => 0];
    const DEFAULT_WIDTH = 100;
    const DEFAULT_HEIGHT = 100;
    
    private $origin;
    private $width;
    private $height;
    private $template;
    private $svg;
    private $avatar;

    public function __construct($template) {
        $this->origin = self::DEFAULT_ORIGIN;
        $this->width = self::DEFAULT_WIDTH;
        $this->height = self::DEFAULT_HEIGHT;
        $this->template = $template;
        $this->svg = '';
        $this->avatar = null;
    }

    public function setAvatar($avatar) {
        $this->avatar = $avatar; // Objet de la classe AvatarMatrix
    }

    public function drawSvg() {
        return $this->svg;
    }

    public function writeSvg() {
        // Début de la temporisation :
        ob_start();
        // Ecriture du SVG :
        include($this->template);
        // Récupération du contenu de la temporisation :
        $this->svg = ob_get_clean();
    }

    // public function saveSvg() {
    //     $file = fopen('./avatars/avatar.svg', 'w');
    //     fwrite($file, $this->svg);
    //     fclose($file);
    // }

    // GETTERS
    public function getOrigin($axe) {
        $axe = strtolower($axe);
        return $this->origin[$axe];
    }

    public function getWidth() {
        return $this->width;
    }

    public function getHeight() {
        return $this->height;
    }
}