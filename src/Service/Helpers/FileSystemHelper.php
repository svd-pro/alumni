<?php
namespace App\Service\Helpers;

class FileSystemHelper {

    private $fileName;
    private $filePath;
    private $fileContent;

    public function __construct() {
        $this->fileName = '';
        $this->filePath = '';
        $this->fileContent = '';
    }

    public function setFileName(string $fileName) {
        $this->fileName = $fileName;
    }

    public function setHalfRandomFileName(string $baseName, string $extension) {
        $randomEnding = str_pad(rand(0, 999), 3, "0", STR_PAD_LEFT);
        $dateTime = date("Y-m-d_H-i-s");
        $this->fileName = $baseName . '_' . $dateTime . '_' . $randomEnding . '.' . $extension;
    }

    public function setFullRandomFileName(string $extension) {
        $randomString = sha1(uniqid(rand(1, 1000000), true)); // Doc uniqid : https://www.php.net/manual/fr/function.uniqid.php
        $this->fileName = $randomString . '.' . $extension;
    }

    public function setFilePath(string $filePath) {
        $filePathExplode = explode('/', $filePath);
        $currentPath = '';
        foreach($filePathExplode as $value) {
            $currentPath .= $value . '/';
            if(!is_dir($currentPath)) { // Si le dossier spécifié n'existe pas
                mkdir($currentPath, 755, true); // PHP le crée
            }
        }
        $this->filePath = $currentPath;
    }

    public function getName() {
        return $this->fileName;
    }

    public function getPathAndName() {
        return $this->filePath . $this->fileName;
    }

    public function saveFile(string $fileContent) {
        $file = fopen($this->filePath . $this->fileName, 'w');
        fwrite($file, $fileContent);
        fclose($file);
    }

}