<?php
namespace App\Service\Tools;

// Course name : ColorTools
class ColorsProvider {
    
    static function getRandomColor() {
        $hexaValues = str_split('ABCDEF0123456789');
        $color = '#';
        for ($c = 0; $c < 6; $c++) {
            $color .= $hexaValues[rand(0, 15)];
        }
        return $color;
    }

    static function getRandomColors($numberOfColors) {
        $setOfColors = [];
        for ($i = 0; $i < $numberOfColors; $i++) {
            $setOfColors[] = self::getRandomColor();
        }
        return $setOfColors;
    }

}