<svg 
    viewBox="
        <?= $this->getOrigin('x') 
        . ' ' . $this->getOrigin('y') 
        . ' ' . $this->avatar->getSize() 
        . ' ' . $this->avatar->getSize(); ?>" 
    width="
        <?= $this->getWidth(); ?>" 
    height="
        <?= $this->getHeight(); ?>" 
    xmlns="http://www.w3.org/2000/svg">
    <?php foreach($this->avatar->getMatrix() as $y => $col): ?>
        <?php foreach($col as $x => $color): ?>
            <rect x="<?= $x; ?>" y="<?= $y; ?>"
            width="1" height="1"
            fill="<?= $color; ?>" />
        <?php endforeach; ?>
    <?php endforeach; ?>
</svg>